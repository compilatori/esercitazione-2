/* JFlex stub di riferimento:https://inst.eecs.berkeley.edu/~cs164/fa06/docs/jflex/manual.pdf */
import java_cup.runtime.Symbol;
/**
 * This class is a simple example lexer.
 */
%%
%class Lexer
%cupsym Token
%unicode
%cup
%line
%column
%{
    StringBuffer string = new StringBuffer();

    private Symbol createToken(int type) {
        return new Symbol(type);
    }
    private Symbol createToken(int type, Object value) {
        return new Symbol(type, value);
    }
    private Symbol createError() {
        return new Symbol(Token.ERROR);
    }
    private Symbol createError(Object value) {
        return new Symbol(Token.ERROR, value);
    }

    // inizializza il reader per lettura e controlla errori
      public boolean initialize(String filePath) {
        try {
          /** the input device */
          this.zzReader = new java.io.FileReader(filePath);
          return true;
        } catch (java.io.FileNotFoundException e) {
          return false;
        }
      }

      Lexer() { }

%}
LineTerminator = \r|\n|\r\n
InputCharacter = [^\r\n]
WhiteSpace     = {LineTerminator} | [ \t\f]

/* comments */
Comment = {TraditionalComment} | {EndOfLineComment} | {DocumentationComment}

TraditionalComment   = "/*" [^*] ~"*/" | "/*" "*"+ "/"
EndOfLineComment     = "//" {InputCharacter}* {LineTerminator}
DocumentationComment = "/**" {CommentContent} "*"+ "/"
CommentContent       = ( [^*] | \*+ [^/*] )*

Identifier = [:jletter:] [:jletterdigit:]*

IntegerLiteral = 0 | [1-9][0-9]*
FloatLiteral = (0 | [1-9][0-9]*)\.[0-9]+


%%

// The lexical state YYINITIAL is predefined and is also the state in which the lexer begins scanning
<YYINITIAL> {
  /* keywords */
  "if"                { return createToken(Token.IF); }
  "then"              { return createToken(Token.THEN); }
  "else"              { return createToken(Token.ELSE); }
  "while"             { return createToken(Token.WHILE); }

  /* identifiers */
  {Identifier}         { return createToken(Token.ID, yytext()); } //yytext is used to get the next token from the input

  /* literals */
  {IntegerLiteral}     { return createToken(Token.INT,Integer.parseInt(yytext())); }
  {FloatLiteral}       { return createToken(Token.FLOAT,Double.parseDouble(yytext())); }


  /* comments */
  {Comment}            { /* ignore */ }

  /* whitespace */
  {WhiteSpace}         { /* ignore */ }

  /* operators */
  "<"                  { return createToken(Token.LT); }
  "<="                 { return createToken(Token.LE); }
  "="                 { return createToken(Token.EQUAL); }
  "<>"                 { return createToken(Token.NE); }
  ">"                  { return createToken(Token.GT); }
  ">="                 { return createToken(Token.GE); }
  "<--"                  { return createToken(Token.ASSIGN); }

  /* separators */
  "," { return createToken(Token.COMMA); }
  ";" { return createToken(Token.SEMI); }

  /* Parenthesis and bracket */
  "(" { return createToken(Token.LPAR); }
  ")" { return createToken(Token.RPAR); }
  "{" { return createToken(Token.LBRA); }
  "}" { return createToken(Token.RBRA); }

}

/* END OF FILE */
<<EOF>> { return createToken(Token.EOF); }


/* error fallback */
[^] {
    System.err.println("Invalid character <" + yytext() + "> on LINE: " + yyline + " COLUMN: " + yycolumn);
    return createError(yytext());
}